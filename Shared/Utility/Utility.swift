//
//  Utility.swift
//  ReddiBro
//
//  Created by Alexey Lizenko on 7/6/17.
//  Copyright © 2017 Alexey Lizenko. All rights reserved.
//

import Foundation

public typealias JSONAny = Any
public typealias JSONDictionary = Dictionary<String, AnyObject>
public typealias JSONArray = Array<AnyObject>
