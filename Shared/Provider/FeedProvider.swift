//
//  FeedProvider.swift
//  ReddiBro
//
//  Created by Alexey Lizenko on 6/6/17.
//  Copyright © 2017 Alexey Lizenko. All rights reserved.
//

import Foundation

class FeedProvider {
    private let networkManager: NetworkManager!
    private let subreddit: NetworkManager.Subreddit
    private var paginator = Paginator()
    
    public private(set) var entities = Array<RedditEntity>()
    
    
    public init(withSubreddit inSubreddit: NetworkManager.Subreddit, networkManager inNetworkManager: NetworkManager) {
        networkManager = inNetworkManager
        subreddit = inSubreddit
    }
    
    func reset() {
        paginator = Paginator()
        entities.removeAll()
    }
    
    var isMoreAvailable: Bool {
        get {
            return (paginator.after != nil)
        }
    }
    
    func fetchContent(_ completion: @escaping((Result<[RedditEntity]>) -> Void)) {
        networkManager.fetch(subreddit: subreddit, paginator: paginator) { (result: Result<([RedditEntity], Paginator)>) in
            switch result {
            case .failure(let error):
                completion(Result(error: error))
            case .success(let (elements, paginator)):
                self.paginator = paginator
                self.entities.append(contentsOf: elements)
                
                completion(Result(elements))
            }
        }
    }
}
