//
//  RedditEntitiy.swift
//  ReddiBro
//
//  Created by Alexey Lizenko on 7/6/17.
//  Copyright © 2017 Alexey Lizenko. All rights reserved.
//

import Foundation


class RedditEntity: CustomStringConvertible {
    enum Kind: String {
        case comment = "t1"
        case account = "t2"
        case link = "t3"
        case message = "t4"
        case subreddit = "t5"
        case award = "t6"
        
        case unknown = ""
    }

    
    let id: String
    let kind: Kind!
    let title: String
    let author: String
    let domain: String
    let createdUTC: Int
    let commentsCount: Int
    let thumbnail: URL?
    let image: URL?
    
    var createdDate: Date {
        get {
            return Date.init(timeIntervalSince1970: TimeInterval(createdUTC))
        }
    }
    
    init(withJSON json: JSONDictionary) {
        let kindString = json["kind"] as? String ?? ""
        kind = Kind(rawValue: kindString)!
        
        let dataJSON = json["data"] as? JSONDictionary ?? [:]
        
        id = dataJSON["id"] as? String ?? ""
        title = dataJSON["title"] as? String ?? ""
        author = dataJSON["author"] as? String ?? ""
        createdUTC = dataJSON["created_utc"] as? Int ?? 0
        commentsCount = dataJSON["num_comments"] as? Int ?? 0
        domain = dataJSON["domain"] as? String ?? ""
        if let thumbnailLink = dataJSON["thumbnail"] as? String {
            thumbnail = URL(string: thumbnailLink)
        }
        else {
            thumbnail = nil
        }
        
        var imageURL: URL? = nil
        if let preview = dataJSON["preview"] as? JSONDictionary {
            if let images = preview["images"] as? JSONArray {
                if let imageStruct = images.first as? JSONDictionary {
                    if let imageSource = imageStruct["source"] as? JSONDictionary {
                        if let imageLink = imageSource["url"] as? String {
                            imageURL = URL(string: imageLink)
                        }
                    }
                }
            }
        }
        image = imageURL
    }
    
    var description: String {
        return "\(String(describing: type(of: self))): \(id), \(title)"
    }
}
