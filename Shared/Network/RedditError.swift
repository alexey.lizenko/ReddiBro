//
//  RedditError.swift
//  ReddiBro
//
//  Created by Alexey Lizenko on 7/6/17.
//  Copyright © 2017 Alexey Lizenko. All rights reserved.
//

import Foundation

enum RedditError: Error {
    case unknown
    case unableToFormRequest
    case unableToParseString(Data)
    case unableToParseJSON(JSONAny)
}


struct HTTPError<T>: Error {
    let code: Int
    let object: T
    
    init(withCode code: Int, object: T) {
        self.code = code
        self.object = object
    }
}
