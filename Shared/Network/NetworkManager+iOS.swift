//
//  NetworkManager+iOS.swift
//  ReddiBro-iOS
//
//  Created by Alexey Lizenko on 8/31/17.
//  Copyright © 2017 Alexey Lizenko. All rights reserved.
//

import UIKit

extension NetworkManager {
    func fetchImage(_ url: URL, completion: @escaping (Result<UIImage>) -> Void) {
        let urlRequest = URLRequest(url: url)
        
        let task = session.dataTask(with: urlRequest, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) in
            let result = Result(Response(data: data, urlResponse: response), error: error as NSError?)
                .flatMap(self.responseToData)
                .flatMap(self.dataToImage)
            DispatchQueue.main.async {
                completion(result)
            }
        })
        task.resume()
    }
}
