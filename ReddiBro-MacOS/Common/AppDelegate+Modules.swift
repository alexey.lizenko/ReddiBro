//
//  AppDelegate+Modules.swift
//  ReddiBro
//
//  Created by Alexey Lizenko on 6/6/17.
//  Copyright © 2017 Alexey Lizenko. All rights reserved.
//

import Cocoa

extension AppDelegate {
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        for module in modules {
            module.applicationDidFinishLaunching?(aNotification)
        }
    }
    
    func applicationWillTerminate(_ aNotification: Notification) {
        for module in modules {
            module.applicationWillTerminate?(aNotification)
        }
    }

    func applicationShouldHandleReopen(_ sender: NSApplication, hasVisibleWindows flag: Bool) -> Bool {
        var result = true
        for module in modules {
            result = result && module.applicationShouldHandleReopen?(sender, hasVisibleWindows: flag) ?? true
        }
        
        return result
    }
}
