//
//  NetImageView.swift
//  ReddiBro
//
//  Created by Alexey Lizenko on 7/6/17.
//  Copyright © 2017 Alexey Lizenko. All rights reserved.
//

import Cocoa

class NetImageView: NSImageView {
    
    var loadingTask: URLSessionDataTask?
    
    deinit {
        loadingTask?.cancel()
    }
    
    func setImage(withURL url: URL, placeholder: NSImage, complition: ((Bool)->())? = nil) {
        if let loadingTask = self.loadingTask {
            loadingTask.cancel()
        }
        
        self.image = placeholder
        
        loadingTask = URLSession.shared.dataTask(with: url, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) in
            if let dataVal = data {
                let image = NSImage.init(data: dataVal)
                DispatchQueue.main.async {
                    self.image = image
                    complition?(true)
                }
            }
            else {
                complition?(false)
            }
        })
        loadingTask?.resume()
    }
    
}
