//
//  ImagePreviewWindowController.swift
//  ReddiBro
//
//  Created by Alexey Lizenko on 6/13/17.
//  Copyright © 2017 Alexey Lizenko. All rights reserved.
//

import Cocoa

class ImagePreviewWindowController: NSWindowController {
    var imageURL: URL? {
        didSet {
            mainViewController?.imageURL = imageURL
        }
    }
    
    var mainViewController: ImagePreviewViewController? {
        return self.contentViewController as! ImagePreviewViewController?
    }

    class func instantiateWindowController(withImageURL imageURL: URL) -> ImagePreviewWindowController {
        let storyboard = NSStoryboard.init(name: "ImagePreview", bundle: nil)
        let windowController = storyboard.instantiateInitialController() as! ImagePreviewWindowController
        windowController.imageURL = imageURL
        
        return windowController
    }
}
