//
//  RedditFeedTableViewController.swift
//  ReddiBro
//
//  Created by Alexey Lizenko on 7/6/17.
//  Copyright © 2017 Alexey Lizenko. All rights reserved.
//

import Cocoa

class RedditFeedCell: NSTableCellView {
    @IBOutlet weak var titleLabel: NSTextField?
    @IBOutlet weak var authorLabel: NSTextField?
    @IBOutlet weak var raitingLabel: NSTextField?
    @IBOutlet weak var timeLabel: NSTextField?
    @IBOutlet weak var thumbnailView: NetImageView?
}

class RedditProgressCell: NSTableCellView {
    @IBOutlet weak var progressIndicator: NSProgressIndicator!
}



class RedditFeedTableViewController: NSViewController, NSTableViewDelegate, NSTableViewDataSource {
    var provider: FeedProvider?
    var isLoading: Bool = false
    weak var delegate: RoutingDelegate?
    
    @IBOutlet var tableView: NSTableView!
    
    struct CellId {
        static let reddit = "RedditCell"
        static let progress = "ProgressCell"
    }
    
    override func viewWillAppear() {
         super.viewWillAppear()
        
        self.tableView.reloadData()
        provider?.fetchContent { _ in
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLayout() {
        super.viewDidLayout()
        
        self.resizeMainColumn()
    }

    func resizeMainColumn() {
        let column = tableView.tableColumns[0]
        column.width = tableView.bounds.size.width - tableView.intercellSpacing.width
    }
    
    func numberOfRows(in tableView: NSTableView) -> Int {
        var count = provider?.entities.count ?? 0
        count += (provider?.isMoreAvailable ?? false) ? 1 : 0
        
        return count
    }
    
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        let cell: NSView?
        
        if row < provider?.entities.count ?? 0 {
            let contentCell = tableView.make(withIdentifier: CellId.reddit, owner: self) as! RedditFeedCell
            if let redditEntity = provider?.entities[row] {
                contentCell.titleLabel?.stringValue = redditEntity.title
                contentCell.authorLabel?.stringValue = redditEntity.author
                contentCell.raitingLabel?.stringValue = "\(redditEntity.commentsCount)"
                
                contentCell.timeLabel?.stringValue = redditEntity.createdDate.localizedTimePeriodFromNow
                if let thumbnail = redditEntity.thumbnail {
                    contentCell.thumbnailView?.setImage(withURL: thumbnail, placeholder: NSImage(named: "placeholder")!)
                }
            }
            cell = contentCell
        }
        else {
            let progressCell = tableView.make(withIdentifier: CellId.progress, owner: self) as! RedditProgressCell
            progressCell.progressIndicator.startAnimation(self)
            cell = progressCell
            
            if !isLoading {
                provider?.fetchContent { _ in
                    self.isLoading = false
                    self.tableView.reloadData()
                }
            }
            
            isLoading = true
        }

        return cell
    }
    
    @IBAction func doubleClickAction(_ sender: Any) {
        let row = self.tableView.selectedRow
        if let entity = self.provider?.entities[row] {
            delegate?.showPreviewForItem(entity, source: self)
        }
    }
    
    
    func tableViewSelectionDidChange(_ notification: Notification) {
        let row = self.tableView.selectedRow
        if let entity = self.provider?.entities[row] {
            delegate?.didSelecItem(entity, source: self)
        }
    }
}


