//
//  AppModule.swift
//  ReddiBro
//
//  Created by Alexey Lizenko on 6/6/17.
//  Copyright © 2017 Alexey Lizenko. All rights reserved.
//

import Cocoa

protocol AppModule: NSApplicationDelegate {
    
}
