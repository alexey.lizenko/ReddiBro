//
//  MainModule.swift
//  ReddiBro
//
//  Created by Alexey Lizenko on 6/6/17.
//  Copyright © 2017 Alexey Lizenko. All rights reserved.
//

import Cocoa

class MainModule: NSObject, AppModule {
    var router: MainRouter? = nil
    var networkManager = NetworkManager()
    
    func applicationDidFinishLaunching(_ notification: Notification) {
        router = MainRouter(withNetworkManager: networkManager)
        router?.setupWindowStack()
    }
    
    func applicationShouldHandleReopen(_ sender: NSApplication, hasVisibleWindows flag: Bool) -> Bool {
        if !flag {
            router?.showWindow()
            
            return false // false stands for modified behorior
        }
        return true
    }
}
