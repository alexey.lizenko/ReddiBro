//
//  MainModule.swift
//  ReddiBro
//
//  Created by Alexey Lizenko on 6/5/17.
//  Copyright © 2017 Alexey Lizenko. All rights reserved.
//

import UIKit


class MainAppModule : NSObject, AppModule {
    var window: UIWindow?
    var router: MainRouter?
    var networkManager: NetworkManager
    
    init(networkManager inNetworkManager: NetworkManager) {
        self.networkManager = inNetworkManager
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        if let window = window {
            router = MainRouter(withWindow: window, networkManager: networkManager)
            router?.setupViewStack()
        }
        
        return true
    }
}
