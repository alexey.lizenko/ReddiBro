//
//  NetImageView.swift
//  ReddiBro-iOS
//
//  Created by Alexey Lizenko on 8/28/17.
//  Copyright © 2017 Alexey Lizenko. All rights reserved.
//

import UIKit

class NetImageView: UIImageView {
    var loadingTask: URLSessionDataTask?
    
    deinit {
        loadingTask?.cancel()
    }
    
    func loadImage(withURL url: URL, placeholder: UIImage, complition: ((Bool)->())? = nil) {
        if let loadingTask = loadingTask {
            loadingTask.cancel()
        }
        
        self.image = placeholder
        
        loadingTask = URLSession.shared.dataTask(with: url, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) in
            if let dataVal = data {
                let image = UIImage(data: dataVal)
                DispatchQueue.main.async {
                    self.image = image
                    complition?(true)
                }
            }
            else {
                complition?(false)
            }
        })
        loadingTask?.resume()
    }
    
    func cancelLoading() {
        loadingTask?.cancel()
        loadingTask = nil
    }
}
