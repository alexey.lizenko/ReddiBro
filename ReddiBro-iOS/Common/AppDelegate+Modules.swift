//
//  AppDelegate+Modules.swift
//  ReddiBro
//
//  Created by Alexey Lizenko on 6/5/17.
//  Copyright © 2017 Alexey Lizenko. All rights reserved.
//

import UIKit


extension AppDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        for module in modules {
            let result = module.application?(application, didFinishLaunchingWithOptions: launchOptions) ?? true
            if !result {
                break
            }
        }
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        for module in modules {
            module.applicationWillResignActive?(application)
        }
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        for module in modules {
            module.applicationDidEnterBackground?(application)
        }
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        for module in modules {
            module.applicationWillEnterForeground?(application)
        }

    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        for module in modules {
            module.applicationDidBecomeActive?(application)
        }
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        for module in modules {
            module.applicationWillTerminate?(application)
        }
    }
}
